package ci.gitlab.example.persistence

import cats.effect._
import com.typesafe.config.Config
import doobie.hikari.HikariTransactor
import ci.gitlab.example.persistence.mongo.{MongoConfig, MongoLoader}
import ci.gitlab.example.persistence.postgres.{PostgresConfig, TransactorLoader}
import ci.gitlab.example.util.error.ErrorHandle
import ci.gitlab.example.util.syntax.config._
import ci.gitlab.example.util.logging.TraceProvider
import org.mongodb.scala.MongoDatabase

class PersistenceModuleLoader[F[_]: Sync: ErrorHandle: TraceProvider](
    mongoLoader: MongoLoader[F],
    transactorLoader: TransactorLoader[F]
) {

  def load(rootConfig: Config, blocker: Blocker): Resource[F, PersistenceModule[F]] =
    for {
      mongoDatabase <- loadMongoDatabase(rootConfig)
      transactor    <- loadTransactor(rootConfig, blocker)
    } yield PersistenceModule(mongoDatabase, transactor)

  private[persistence] def loadMongoDatabase(rootConfig: Config): Resource[F, MongoDatabase] =
    for {
      mongoConfig <- Resource.liftF(rootConfig.loadF[F, MongoConfig]("application.persistence.mongodb"))
      db          <- mongoLoader.createAndVerify(mongoConfig)
    } yield db

  private[persistence] def loadTransactor(rootConfig: Config, blocker: Blocker): Resource[F, HikariTransactor[F]] =
    for {
      config <- Resource.liftF(rootConfig.loadF[F, PostgresConfig]("application.persistence.postgres"))
      db     <- transactorLoader.createAndVerify(config, blocker)
    } yield db

}

object PersistenceModuleLoader {

  def default[F[_]: Concurrent: Timer: ContextShift: ErrorHandle: TraceProvider] =
    new PersistenceModuleLoader[F](MongoLoader.default, TransactorLoader.default)

}
