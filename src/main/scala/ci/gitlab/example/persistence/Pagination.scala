package ci.gitlab.example.persistence

import ci.gitlab.example.util.logging.Loggable
import eu.timepit.refined.auto._
import eu.timepit.refined.types.numeric.{NonNegInt, PosInt}

@scalaz.deriving(Loggable)
sealed trait Pagination

object Pagination {

  final case object NoPagination                             extends Pagination
  final case class Skip(skip: NonNegInt)                     extends Pagination
  final case class Limit(limit: PosInt)                      extends Pagination
  final case class SkipLimit(skip: NonNegInt, limit: PosInt) extends Pagination

}
