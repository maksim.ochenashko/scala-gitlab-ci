import sbt._

object Settings {
  val organization = "ci.gitlab.example"
  val name         = "scala-gitlab-ci"
}

object Versions {
  val scala            = "2.12.10"
  val http4s           = "0.20.11"
  val circe            = "0.12.1"
  val circeExtras      = "0.12.2"
  val circeConfig      = "0.7.0"
  val cats             = "2.0.0"
  val catsEffect       = "2.0.0"
  val catsMTL          = "0.7.0"
  val monix            = "3.0.0"
  val mongoScalaDriver = "2.7.0"
  val netty            = "4.1.30.Final"
  val doobie           = "0.8.2"
  val scalazDeriving   = "1.0.0"
  val newtype          = "0.4.3"
  val refined          = "0.9.10"
  val sourcecode       = "0.1.7"
  val magnolia         = "0.11.0"
  val logback          = "1.2.3"
  val scalaLogging     = "3.9.2"
  val scalatest        = "3.0.8"
  val catsScalatest    = "3.0.0"
  val scalacheck       = "1.14.2"
}

object Dependencies {

  val root: Seq[ModuleID] = Seq(
    "org.http4s"                 %% "http4s-dsl"           % Versions.http4s,
    "org.http4s"                 %% "http4s-blaze-server"  % Versions.http4s,
    "org.http4s"                 %% "http4s-circe"         % Versions.http4s,
    "io.monix"                   %% "monix"                % Versions.monix,
    "org.typelevel"              %% "cats-core"            % Versions.cats,
    "org.typelevel"              %% "cats-effect"          % Versions.catsEffect,
    "org.typelevel"              %% "cats-mtl-core"        % Versions.catsMTL,
    "org.scalaz"                 %% "deriving-macro"       % Versions.scalazDeriving,
    "io.circe"                   %% "circe-generic"        % Versions.circe,
    "io.circe"                   %% "circe-refined"        % Versions.circe,
    "io.circe"                   %% "circe-generic-extras" % Versions.circeExtras,
    "io.circe"                   %% "circe-config"         % Versions.circeConfig,
    "io.estatico"                %% "newtype"              % Versions.newtype,
    "eu.timepit"                 %% "refined"              % Versions.refined,
    "eu.timepit"                 %% "refined-cats"         % Versions.refined,
    "org.tpolecat"               %% "doobie-hikari"        % Versions.doobie,
    "org.tpolecat"               %% "doobie-refined"       % Versions.doobie,
    "org.tpolecat"               %% "doobie-postgres"      % Versions.doobie,
    "org.mongodb.scala"          %% "mongo-scala-driver"   % Versions.mongoScalaDriver,
    "io.netty"                   % "netty-all"             % Versions.netty,
    "com.lihaoyi"                %% "sourcecode"           % Versions.sourcecode,
    "com.propensive"             %% "magnolia"             % Versions.magnolia,
    "com.typesafe.scala-logging" %% "scala-logging"        % Versions.scalaLogging,
    "ch.qos.logback"             % "logback-classic"       % Versions.logback,
    "org.tpolecat"               %% "doobie-scalatest"     % Versions.doobie % "it",
    "org.scalatest"              %% "scalatest"            % Versions.scalatest % "it,test",
    "com.ironcorelabs"           %% "cats-scalatest"       % Versions.catsScalatest % "it,test",
    "org.scalacheck"             %% "scalacheck"           % Versions.scalacheck % "it,test",
    "eu.timepit"                 %% "refined-scalacheck"   % Versions.refined % "it,test"
  )

}
