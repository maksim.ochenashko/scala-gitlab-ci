package ci.gitlab.example.util

import ci.gitlab.example.util.config.ToConfigOps
import ci.gitlab.example.util.error.ToErrorRaiseOps
import ci.gitlab.example.util.execution.ToRetryOps
import ci.gitlab.example.util.json.ToJsonOps
import ci.gitlab.example.util.logging.Loggable.InterpolatorOps
import ci.gitlab.example.util.syntax.mtl.ToAllMtlOps

object syntax {

  object all     extends ToConfigOps with ToJsonOps with InterpolatorOps with ToRetryOps with ToAllMtlOps
  object config  extends ToConfigOps
  object json    extends ToJsonOps
  object logging extends InterpolatorOps
  object retry   extends ToRetryOps

  object mtl {
    private[syntax] trait ToAllMtlOps extends ToErrorRaiseOps

    object all   extends ToAllMtlOps
    object raise extends ToErrorRaiseOps
  }

}
