package ci.gitlab.example.test

import cats.data.EitherT
import cats.effect.Concurrent
import cats.scalatest.{EitherMatchers, EitherValues}
import cats.syntax.functor._
import ci.gitlab.example.util.ClassUtils
import ci.gitlab.example.util.execution.Traced
import ci.gitlab.example.util.logging.TraceId
import monix.eval.Task
import monix.execution.Scheduler
import org.scalatest.{Inside, Matchers, OptionValues, WordSpecLike}
import org.scalatestplus.scalacheck.{CheckerAsserting, ScalaCheckPropertyChecks}

import scala.concurrent.duration._

abstract class EffectSpec[E]
    extends WordSpecLike
    with Matchers
    with EitherMatchers
    with OptionValues
    with Inside
    with EitherValues
    with ScalaCheckPropertyChecks {

  protected type Eff[A] = Traced[EitherT[Task, E, ?], A]

  protected implicit val DefaultScheduler: Scheduler = monix.execution.Scheduler.Implicits.global
  protected implicit val Eff: Concurrent[Eff]        = Concurrent.catsKleisliConcurrent

  protected final val DefaultTimeout: FiniteDuration = 20.seconds

  object EffectAssertion {

    @SuppressWarnings(Array("org.wartremover.warts.DefaultArguments"))
    def apply[A](timeout: Duration = DefaultTimeout)(program: Eff[A]): Unit =
      (for {
        traceId <- TraceId.randomAlphanumeric[Task](className)
        result  <- program.run(traceId).void.value
      } yield result).runSyncUnsafe(timeout).value

  }

  protected implicit def checkingAsserting[A]: CheckerAsserting[A] { type Result = Eff[Unit] } =
    new EffectCheckerAsserting[Eff, A]

  private lazy val className: String = ClassUtils.getClassSimpleName(getClass)

}
