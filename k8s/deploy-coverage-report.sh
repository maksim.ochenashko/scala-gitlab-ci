helm upgrade --install $COVERAGE_RELEASE \
  --namespace=$KUBE_NAMESPACE \
  --set imagePullSecrets=$REG_SECRET \
  --set image.name=$COVERAGE_IMAGE \
  --set ingress.host=$COVERAGE_HOST \
  --set name=$COVERAGE_RELEASE \
  --set gitlabEnv=$CI_ENVIRONMENT_SLUG \
  --set gitlabApp=$CI_PROJECT_PATH_SLUG \
  --wait \
  k8s/coverage-report-chart