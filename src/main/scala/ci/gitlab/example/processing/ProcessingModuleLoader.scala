package ci.gitlab.example.processing

import cats.effect._
import ci.gitlab.example.persistence.PersistenceModule
import com.typesafe.config.Config

class ProcessingModuleLoader[F[_]: Sync] {

  def load(rootConfig: Config, persistenceModule: PersistenceModule[F]): Resource[F, ProcessingModule[F]] = {
    val _ = (rootConfig, persistenceModule)
    Resource.pure(ProcessingModule[F]())
  }

}
