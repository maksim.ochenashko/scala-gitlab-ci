package ci.gitlab.example.persistence.postgres

import ci.gitlab.example.util.logging.Loggable
import ci.gitlab.example.util.error.ThrowableExtractor

@scalaz.deriving(Loggable)
sealed trait PostgresError

object PostgresError {

  final case class UnavailableConnection(cause: Throwable) extends PostgresError

  final case class ConnectionAttemptTimeout(message: String) extends PostgresError

  def unavailableConnection(cause: Throwable): PostgresError   = UnavailableConnection(cause)
  def connectionAttemptTimeout(message: String): PostgresError = ConnectionAttemptTimeout(message)

  implicit val postgresErrorThrowableExtractor: ThrowableExtractor[PostgresError] = {
    case _: ConnectionAttemptTimeout  => None
    case UnavailableConnection(cause) => Option(cause)
  }

}
