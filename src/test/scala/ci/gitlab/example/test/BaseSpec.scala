package ci.gitlab.example.test

import ci.gitlab.example.util.error.RaisedError

trait BaseSpec extends EffectSpec[RaisedError]
